#include <iostream>
#include "Piece.h"

int main()
{
	Piece piece(Piece::Body::FULL, Piece::Color::DARK, Piece::Height::SHORT, Piece::Shape::ROUND);

	std::cout << piece;

	return 0;
}