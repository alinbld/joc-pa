#pragma once
#include <iostream>

class Piece
{
	//O piesa o sa aiba urmatoarele enum-uri: body, color, height, shape

public:
	enum class Body {
		FULL, 
		HOLLOW
	};
	enum class Color {
		DARK,
		LIGHT
	};
	enum class Height {
		SHORT,
		TALL
	};
	enum class Shape {
		ROUND,
		SQUARE
	};

private:
	Body m_body;
	Color m_color;
	Height m_height;
	Shape m_shape;

public:
	Piece(Body, Color, Height, Shape);

	Body getBody() const;
	Color getColor() const;
	Height getHeight() const;
	Shape getShape() const;

	friend std::ostream& operator<<(std::ostream&, const Piece&);
};

