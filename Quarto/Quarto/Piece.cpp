#include "Piece.h"

Piece::Piece(Body body, Color color, Height height, Shape shape)
{
	m_body = body;
	m_color = color;
	m_height = height;
	m_shape = shape;
}

Piece::Body Piece::getBody() const
{
	return Body();
}

Piece::Color Piece::getColor() const
{
	return Color();
}

Piece::Height Piece::getHeight() const
{
	return Height();
}

Piece::Shape Piece::getShape() const
{
	return Shape();
}

std::ostream& operator<<(std::ostream& out, const Piece& piece)
{
	//out << static_cast<int>(piece.m_body) << " " << static_cast<int>(piece.m_color) << " " << static_cast<int>(piece.m_height) << " " << static_cast<int>(piece.m_shape) << std::endl;

	if (piece.getBody() == Piece::Body::FULL)
		out << "FULL ";
	else
		out << "HOLLOW ";

	if (piece.getColor() == Piece::Color::DARK)
		out << "DARK ";
	else
		out << "LIGHT ";

	if (piece.getHeight() == Piece::Height::TALL)
		out << "TALL ";
	else
		out << "SHORT ";

	if (piece.getShape() == Piece::Shape::ROUND)
		out << "ROUND ";
	else
		out << "SQUARE ";

	out << std::endl;

	return out;
}
